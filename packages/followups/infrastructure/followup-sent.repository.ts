import { FollowupSentAggregate } from '@domain/entities/followup-sent.aggregate';

export interface FollowupSentRepository {
  persist(aggregate: FollowupSentAggregate): Promise<void>;
}

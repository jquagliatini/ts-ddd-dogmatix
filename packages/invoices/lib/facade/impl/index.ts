import { Injectable } from '@nestjs/common';
import { InvoicePayableAggregate } from '../../domain/entities/invoice-payable.aggregate';
import { InvoicesFacade } from '../types';

@Injectable()
export class InvoicesFacadeImpl implements InvoicesFacade {
  private static byNumber: Map<string, InvoicePayableAggregate> = new Map();

  persist(number: string, invoice: InvoicePayableAggregate) {
    InvoicesFacadeImpl.byNumber.set(number, invoice);
  }

  getPayableByNumber(number: string): Promise<InvoicePayableAggregate | null> {
    return Promise.resolve(
      InvoicesFacadeImpl.byNumber.get(number) ?? null
    );
  }
}

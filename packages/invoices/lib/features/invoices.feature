Feature: Invoice

  Scenario: I create an invoice
    Given I create an invoice with the following
      """
      { "invoiceAmount": 100000 }
      """
    And I paid the given invoice 100.00 in the last 100 days
    Then I should have an invoice with
      """
      { "paidAmount": 10000, "invoiceAmount": 100000 }
      """

import { Given } from '@cucumber/cucumber';
import { faker } from '@faker-js/faker/locale/en';

Given(
  'I paid the given invoice {float} in the last {int} days',
  function (float, int) {
    this.invoice = this.invoice.pay({
      amountCents: float * 100,
      date: faker.date.recent(int)
    });
  },
);

import { z } from 'zod';
import { Amount, amountValidator } from './amount.type';

export const paymentAmountValidator = amountValidator
export type PaymentAmount = Amount;

export const paymentDateValidator = z.date();
export type PaymentDate = z.infer<typeof paymentDateValidator>;

export const invoicePaymentValidator = z.object({
  amountCents: paymentAmountValidator,
  date: paymentDateValidator,
});
export type InvoicePayment = z.infer<typeof invoicePaymentValidator>;

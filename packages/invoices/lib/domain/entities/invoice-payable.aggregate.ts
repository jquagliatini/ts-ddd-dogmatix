import { z } from 'zod';
import { Amount, amountValidator } from '../types/amount.type';
import {
  InvoicePayment,
  invoicePaymentValidator,
  PaymentAmount,
  paymentAmountValidator,
} from '../types/payment.type';
import { InvoicePaidAggregate } from './invoice-paid.aggregate';

export class InvoicePayableAggregate {
  private constructor(
    readonly paidAmount: PaymentAmount,
    readonly invoiceAmount: Amount,
  ) {}

  static from(maybeProps: InvoicePayableAggregateFromProps) {
    const props = invoicePayableAggregateFromPropsValidator.parse(maybeProps);
    return new this(props.paidAmount, props.invoiceAmount);
  }

  pay(invoicePayment: InvoicePayment) {
    const payment = invoicePaymentValidator.parse(invoicePayment);
    if (payment.amountCents + this.paidAmount > this.invoiceAmount) {
      throw new PaymentAmountExceedsInvoiceAmount(
        this.paidAmount,
        this.invoiceAmount,
        payment.amountCents,
      );
    }

    return payment.amountCents + this.paidAmount === this.invoiceAmount
      ? new InvoicePaidAggregate()
      : InvoicePayableAggregate.from({
          paidAmount: payment.amountCents + this.paidAmount,
          invoiceAmount: this.invoiceAmount,
        });
  }
}

const invoicePayableAggregateFromPropsValidator = z.object({
  paidAmount: paymentAmountValidator,
  invoiceAmount: amountValidator,
});
export type InvoicePayableAggregateFromProps = z.infer<
  typeof invoicePayableAggregateFromPropsValidator
>;

export class PaymentAmountExceedsInvoiceAmount {
  constructor(
    readonly paidAmount: PaymentAmount,
    readonly invoiceAmount: Amount,
    readonly paymentAmount: PaymentAmount,
  ) {}
}

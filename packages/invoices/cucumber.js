const path = require('node:path');

module.exports = {
  default: {
		paths: [path.join(__dirname, 'lib', 'features', '**', '*.feature')],
    require: [
			path.join(__dirname, 'dist', 'tests', 'steps', '**', '*.step.js'),
    ],
		publishQuiet: true,
  },
};

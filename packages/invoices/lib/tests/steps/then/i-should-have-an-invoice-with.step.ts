import { Then } from "@cucumber/cucumber";
import { expect } from 'expect';

Then('I should have an invoice with', function (docString) {
  expect(this.invoice).toMatchObject(
    JSON.parse(docString ?? '{}'),
  );
});

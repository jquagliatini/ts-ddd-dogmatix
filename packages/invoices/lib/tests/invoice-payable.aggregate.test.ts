import { describe, it } from 'node:test';
import { expect } from 'expect';
import { createInvoicePayable } from './fixtures/invoice-payable.fixture';
import { faker } from '@faker-js/faker/locale/en';

describe('InvoicePayableAggregate', () => {
  it('should prevent paying a negative amount', () => {
    const invoice = createInvoicePayable({ invoiceAmount: 1_000_00 });
    expect(() =>
      invoice.pay({ amountCents: -100_00, date: new Date() }),
    ).toThrow();
  });

  it('should allow to add a payment to an ongoing invoice', () => {
    const invoice = createInvoicePayable({ invoiceAmount: 1_000_00 });
    const next = invoice.pay({
      amountCents: 100_00,
      date: faker.date.recent(100),
    });

    expect(next).toMatchObject({
      paidAmount: 100_00,
      invoiceAmount: 1_000_00,
    });
  });
});

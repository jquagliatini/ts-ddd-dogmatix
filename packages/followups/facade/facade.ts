import { Module } from '@nestjs/common';

export interface FollowupsFacade {
  getAllSentFollowupIds(): Promise<string[]>;
}

export const FOLLOWUPS_FACADE_TOKEN = Symbol();

class FollowupsFacadeImpl implements FollowupsFacade {
  getAllSentFollowupIds(): Promise<string[]> {
    return Promise.resolve([]);
  }
}

@Module({
  providers: [
    { provide: FOLLOWUPS_FACADE_TOKEN, useClass: FollowupsFacadeImpl },
  ]
})
export class FollowupsFacadeModule {}

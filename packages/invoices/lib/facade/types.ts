import { InvoicePayableAggregate } from "../domain/entities/invoice-payable.aggregate";

export interface InvoicesFacade {
  getPayableByNumber(number: string): Promise<InvoicePayableAggregate | null>;
}

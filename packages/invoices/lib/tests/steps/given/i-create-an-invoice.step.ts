import { Given } from '@cucumber/cucumber';
import { createInvoicePayable } from '../../fixtures/invoice-payable.fixture';

Given('I create an invoice with the following', function (docString) {
  this.invoice = createInvoicePayable(JSON.parse(docString ?? '{}'))
});

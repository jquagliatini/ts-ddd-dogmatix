import { z } from 'zod';

export const amountValidator = z.number().int().min(0);
export type Amount = z.infer<typeof amountValidator>;

import { FollowupsFacade } from "@followups";

export class FollowupGateway {
  constructor(
    private readonly followupsFacade: FollowupsFacade
  ) {}

  getFollowups(): Promise<string[]> {
    return this.followupsFacade.getAllSentFollowupIds();
  }
}

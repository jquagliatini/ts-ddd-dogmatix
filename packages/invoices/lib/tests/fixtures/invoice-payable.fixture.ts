import { faker } from '@faker-js/faker';
import { InvoicePayableAggregate, InvoicePayableAggregateFromProps } from "../../domain/entities/invoice-payable.aggregate";

export function createInvoicePayable(props: Partial<InvoicePayableAggregateFromProps> = {}): InvoicePayableAggregate {
  return InvoicePayableAggregate.from({
    invoiceAmount: faker.datatype.number({ min: 1_00, max: 1_000_000_00 }),
    paidAmount: 0,
    ...props
  });
}

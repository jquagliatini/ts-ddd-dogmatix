import { Module } from '@nestjs/common';
import { InvoicesFacadeImpl } from "./impl";
import { INVOICES_FACADE_TOKEN } from './tokens';

@Module({
  providers: [{ provide: INVOICES_FACADE_TOKEN, useClass: InvoicesFacadeImpl }],
  exports: [INVOICES_FACADE_TOKEN]
})
export class InvoicesFacadeModule { }
